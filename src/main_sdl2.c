
#if defined(BACKEND_SDL2) && !(defined(__x86_64__) && defined(USE_INLINE_ASM_STUFF) && !defined(MODE_DUMP))

#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>

#include <SDL.h>
#include <SDL_video.h>
#include <SDL_events.h>
#include <SDL_keyboard.h>
#include <SDL_mouse.h>

#define GL_GLEXT_PROTOTYPES
#include <GL/glx.h>
#include <GL/glext.h>

#include "sys.h"
#include "dbg.h"
#include "intr.h"
#include "loop.h"

#include "gldbg.c"

__attribute__((__used__, __externally_visible__))
int main(int argc, char* argv[]
#ifdef NEED_ENVIRON
        , char* envp[]
#endif
        ) {
#ifndef NO_ERROR_CHECKING
	if (SDL_Init(SDL_INIT_VIDEO|SDL_INIT_AUDIO)) return 0;
#else
	SDL_Init(SDL_INIT_VIDEO|SDL_INIT_AUDIO);
#endif

	SDL_Window* w = SDL_CreateWindow(
#ifdef DESPERATE
			""
#else
			"K2"
#endif
			, 0, 0/*SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED*/
			, CANVAS_WIDTH, CANVAS_HEIGHT, /*SDL_WINDOW_SHOWN|*/SDL_WINDOW_OPENGL
#ifndef DESPERATE_UNSAFE
				|SDL_WINDOW_ALLOW_HIGHDPI
#endif
#ifdef FULLSCREEN
				|SDL_WINDOW_FULLSCREEN
#elif defined(CANVAS_RESIZABLE)
				|SDL_WINDOW_RESIZABLE
#endif
	);
#ifndef NO_ERROR_CHECKING
	if (!w) {
		k2ping("no win\n");
		return 0;
	}
#endif

#ifndef DESPERATE_UNSAFE
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK,
#ifdef USE_CORE_PROFILE
			SDL_GL_CONTEXT_PROFILE_CORE
#else
			SDL_GL_CONTEXT_PROFILE_COMPATIBILITY
#endif
		);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 4);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
#ifdef VERBOSE
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_FLAGS, SDL_GL_CONTEXT_DEBUG_FLAG);
#endif
#endif

	__attribute__((__unused__)) SDL_GLContext ctx = SDL_GL_CreateContext(w);
#ifndef NO_ERROR_CHECKING
	if (!ctx) {
		k2ping("no gl\n");
		return 0;
	}
#endif
	//SDL_GL_MakeCurrent(w, ctx);

#ifdef HIDEMOUSE
	SDL_ShowCursor(SDL_DISABLE);
#endif
#ifdef VSYNC
	SDL_GL_SetSwapInterval(1);
#endif

#ifdef VERBOSE
	glEnable(GL_DEBUG_OUTPUT|GL_DEBUG_OUTPUT_SYNCHRONOUS);
	glDebugMessageCallback(gldbg_cb, NULL);
	glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE,
			0, NULL, GL_TRUE);

	k2dbg("GL v%s/glsl v%s (%s: %s)\n", glGetString(GL_VERSION),
			glGetString(GL_SHADING_LANGUAGE_VERSION),
			glGetString(GL_VENDOR), glGetString(GL_RENDERER));
	k2dbg("GL exts: %s\n", glGetString(GL_EXTENSIONS));
#endif

#ifdef NO_ERROR_CHECKING
	loop_init();
#else
	if (!loop_init()) {
		k2ping("no init\n");
		goto END;
	}
#endif

	int ww = CANVAS_WIDTH, hh = CANVAS_HEIGHT;

	SDL_Event e;
	do {
#if defined(CANVAS_RESIZABLE) && !defined(MODE_DUMP)
		SDL_GL_GetDrawableSize(w, &ww, &hh);
#endif

//#ifdef DESPERATE
		loop_tick(ww, hh);
		SDL_GL_SwapWindow(w);
//#endif

		while (SDL_PollEvent(&e)) {
			if (e.type == SDL_QUIT) goto END;
#ifndef DESPERATE
			else if (e.type == SDL_WINDOWEVENT
					&& e.window.event == SDL_WINDOWEVENT_EXPOSED) {
				loop_tick(ww, hh);
				SDL_GL_SwapWindow(w);
			}
#endif
#ifndef DESPERATE_UNSAFE
			else if (e.type == SDL_KEYDOWN && e.key.keysym.sym == SDLK_ESCAPE)
				goto END;
#endif
		}
	} while (true);

END:

#ifndef DESPERATE_UNSAFE
	loop_exit();
#endif

#ifndef DESPERATE
	SDL_GL_DeleteContext(ctx);
	SDL_DestroyWindow(w);
	SDL_Quit();
#endif

#ifndef DESPERATE_UNSAFE
	return 0;
#else
	return DONT_CARE(int);
#endif
}

#endif

