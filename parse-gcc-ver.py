#!/usr/bin/env python3

import sys

lines = [x.strip() for x in sys.stdin.readlines()]

if len(lines) < 1 or "Free Software Foundation" not in lines[1]:
    exit(1) # prolly clang

verline = lines[0]

parenind, closeind = -1, -1
try:
    parenind = verline.index('(')
    closeind = verline.index(')', parenind)
except: exit(1)
if parenind < 0 or closeind < 0: exit(1)
try:
    xxx = verline.index(')', closeind+1)
    exit(1) # another ) ?
except: pass

command = verline[:parenind-1]
verbose = verline[parenind+1:closeind]
version = verline[closeind+1:].strip().split('.')

#print(command)
#print(verbose)
print(version[0])

