
#ifdef SYNTH_BACKEND_OIDOS

#include <stddef.h>

#include "def.h"
#include "sys.h"

#include "oidos.h"

#include "synth.h"

//extern char** environ;

static const char*const argv_aplay[] = {
	"/usr/bin/aplay",
	NULL
};

void synth_execve(
#ifndef DESPERATE_UNSAFE
		const char* envp[]
#endif
) {
	SYS_execvec(
		argv_aplay[0], argv_aplay,
#ifndef DESPERATE_UNSAFE
			envp
#else
			&argv_aplay[1] // null string
#endif
	);
}

void synth_init(int _) {
	Oidos_FillRandomData();
	Oidos_GenerateMusic();
}

void synth_main(int pipefd) {
#ifndef MODE_DUMP
	SYS_write(pipefd, Oidos_WavFileHeader, sizeof(Oidos_WavFileHeader));
#endif
	SYS_write(pipefd, Oidos_MusicBuffer, Oidos_WavFileHeader[10]);
}

size_t synth_get_outbuf_size() {
	return sizeof(Oidos_MusicBuffer);
}
const void* synth_get_outbuf() {
	return Oidos_WavFileHeader[10];
}

const struct synth_wave_hdr* synth_get_wavhdr() {
	return (const struct synth_wave_hdr*)Oidos_WavFileHeader;
}
void synth_write_wavhdr(int pipefd) {
	SYS_write(pipefd, Oidos_WavFileHeader, sizeof(Oidos_WavFileHeader));
}
#endif /* SYNTH_BACKEND_OIDOS */

