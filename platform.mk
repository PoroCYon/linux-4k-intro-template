
BITS ?= $(shell getconf LONG_BIT)

CC_VERSION := $(shell $(CC) --version)

ifneq ($(findstring Free Software Foundation,$(CC_VERSION)),)
IS_GCC := 1

ifeq ($(shell $(CC) --version | ./parse-gcc-ver.py),9)
IS_GCC_9 := 1
else
IS_GCC_9 := 0
endif
else
IS_GCC   := 0
IS_GCC_9 := 0
endif
ifneq ($(findstring clang,$(CC_VERSION)),)
IS_CLANG := 1
else
IS_CLANG := 0
endif

ifeq ($(BITS),32)
# snd binary
ARCH_CC_OPT := -m32
PKGCONFIG ?= pkg-config-32
SDLCONFIG ?= sdl2-config-32
ARCH_LD_OPT := -m elf_i386
else
ARCH_CC_OPT := -m64
PKGCONFIG ?= pkg-config
SDLCONFIG ?= sdl2-config
ARCH_LD_OPT := -m elf_x86_64
endif

