
#ifndef LOOP_H_
#define LOOP_H_

#include <stdbool.h>

#include "def.h"

lto_inline bool loop_init(void);
lto_inline void loop_tick(int w, int h);
lto_inline void loop_exit(void);

#endif

