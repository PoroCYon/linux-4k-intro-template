
#ifndef FILE_H_
#define FILE_H_

#include <stddef.h>
#include <stdbool.h>

#include "def.h"

struct file_res {
	void* data;
	size_t len;
};

// README: I've defined these two so you can easily hotswap them with some code
//         that can autoreload shaders etc (I've omitted it to keep things
//         simple)
#define file_read(path) \
		((struct file_res){.data = dtn_##path , .len = sizeof(dtn_##path) })\

#define file_is_newer_cont(path, varn, cont) (false)

#define file_open(path) file_read(path)

#endif

