
#ifndef SYS_H_
#define SYS_H_

#include <stddef.h>
#include <unistd.h>

#include <sys/wait.h>
#include <poll.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <time.h>
#include <fcntl.h>
#include <sched.h>

#include "def.h"

#ifndef AT_EMPTY_PATH
#define AT_EMPTY_PATH (0x1000)
#endif

#ifndef PIPE_BUF
#define PIPE_BUF (0x1000)
#endif
#define PIPE_WILL_BLOCK_BUF (4)

force_inline static void SYS_break(void) {
#if defined(__x86_64__) || defined(__i386__)
    // often an illegal instruction on x86 instead of int3
    asm volatile("int3");
#else
    __builtin_trap();
#endif
}

#if (!defined(__x86_64__) && !defined(__i386__)) || !defined(__linux__)
#error "This demo won't work on your platform! Linux on i386 or x86_64 is required!"
#endif

// more repetition than I'd like, but meh
#if defined(__x86_64__)
// D S d r10 r8 r9 (yeah, wat)
#define SYS_syscall0(num) ({\
    register ssize_t __retval asm("rax");\
    asm volatile("push $" STRINGIFY(num) "\npop %%rax\nsyscall\n"\
        :"=a" (__retval) ::"rcx","r11"); \
    __retval;\
})\

#define SYS_syscall1(num, a) ({\
    register ssize_t __retval asm("rax");\
    asm volatile("push $" STRINGIFY(num) "\npop %%rax\nsyscall\n"\
        :"=a" (__retval) \
        : "D" (a) \
        :"rcx","r11"); \
    __retval;\
})\

#define SYS_syscall2(num, a, b) ({\
    register ssize_t __retval asm("rax");\
    asm volatile("push $" STRINGIFY(num) "\npop %%rax\nsyscall\n"\
        :"=a" (__retval) \
        : "D" (a), "S"  (b)\
        :"rcx","r11"); \
    __retval;\
})\

#define SYS_syscall3(num, a, b, c) ({\
    register ssize_t __retval asm("rax");\
    asm volatile("push $" STRINGIFY(num) "\npop %%rax\nsyscall\n"\
        :"=a" (__retval) \
        : "D" (a), "S"  (b), "d" (c)\
        :"rcx","r11"); \
    __retval;\
})\

#define SYS_syscall4(num, a, b, c, d) ({\
    register ssize_t __retval asm("rax");\
    asm volatile( \
        "mov %[fourth], %%r10\n" /* yep, gcc won't let you specify r10 as dest */ \
        "push $" STRINGIFY(num) "\npop %%rax\nsyscall\n"\
        :"=a" (__retval) \
        : "D" (a), "S"  (b), "d" (c), [fourth] "r" ((size_t)d)\
        :"rcx","r11","r10"); \
    __retval;\
})\

#define SYS_syscall5(num, a, b, c, d, e) ({\
    register ssize_t __retval asm("rax");\
    asm volatile( \
        "mov %[fourth], %%r10\n" /* yep, gcc won't let you specify r10 as dest */ \
        "mov %[fifth], %%r8\n" \
        "push $" STRINGIFY(num) "\npop %%rax\n" \
        "syscall\n"\
    :"=a" (__retval) \
    : "D" (a), "S"  (b), "d" (c), [fourth] "c" ((size_t)d), [fifth] "b" ((size_t)e) \
    :"r11","r10","r8"); \
    __retval;\
})\

#define SYS_syscall6(num, a, b, c, d, e, f) ({\
    register ssize_t __retval asm("rax");\
    asm volatile( \
        "mov %[fourth], %%r10\n" /* yep, gcc won't let you specify r10 as dest */ \
        "mov %[fifth], %%r8\n" \
        "mov %[sixth], %%r9\n" \
        "push $" STRINGIFY(num) "\npop %%rax\n" \
        "syscall\n"\
    :"=a" (__retval) \
    : "D" (a), "S"  (b), "d" (c), [fourth] "r" ((size_t)d), [fifth] "r" ((size_t)e), [sixth] "r" ((size_t)f) \
    :"rcx","r11","r10","r8","r9"); \
    __retval;\
})\

#elif defined(__i386__)
// b c d S D
#define SYS_syscall0(num) ({\
    register ssize_t __retval asm("eax");\
    if((num)<128)\
        asm volatile("push $" STRINGIFY(num) "\npop %%eax\nint $0x80\n" \
            :"=a" (__retval)); \
    else\
        asm volatile("mov $"  STRINGIFY(num) ", %%eax\nint $0x80\n"\
            :"=a" (__retval)); \
    __retval; \
})\

#define SYS_syscall1(num, a) ({\
    register ssize_t __retval asm("eax");\
    if ((num)<128)\
        asm volatile("push $" STRINGIFY(num) "\npop %%eax\nint $0x80\n"\
            :"=a" (__retval) :"b" (a):); \
    else\
        asm volatile("mov $"  STRINGIFY(num) ", %%eax\nint $0x80\n"\
            :"=a" (__retval) :"b" (a):); \
    __retval; \
})\

#define SYS_syscall2(num, a, b) ({\
    register ssize_t __retval asm("eax");\
    if((num)<128)\
        asm volatile("push $" STRINGIFY(num) "\npop %%eax\nint $0x80\n"\
            :"=a" (__retval) :"b" (a), "c" (b):); \
    else\
        asm volatile("mov $" STRINGIFY(num) ", %%eax\nint $0x80\n"\
            :"=a" (__retval) :"b" (a), "c" (b):); \
    __retval; \
})\

#define SYS_syscall3(num, a, b, c) ({\
    register ssize_t __retval asm("eax");\
    if((num)<128)\
        asm volatile("push $" STRINGIFY(num) "\npop %%eax\nint $0x80\n"\
            :"=a" (__retval) :"b" (a), "c" (b), "d" (c):); \
    else\
        asm volatile("mov $"  STRINGIFY(num) ", %%eax\nint $0x80\n"\
            :"=a" (__retval) :"b" (a), "c" (b), "d" (c):); \
    __retval; \
})\

#define SYS_syscall4(num, a, b, c, d) ({\
    register ssize_t __retval asm("eax");\
    if((num)<128)\
        asm volatile("push $" STRINGIFY(num) "\npop %%eax\nint $0x80\n"\
            :"=a" (__retval) :"b" (a), "c" (b), "d" (c), "S" (d):); \
    else\
        asm volatile("mov $"  STRINGIFY(num) ", %%eax\nint $0x80\n"\
            :"=a" (__retval) :"b" (a), "c" (b), "d" (c), "S" (d):); \
    __retval; \
})\

#define SYS_syscall5(num, a, b, c, d, e) ({\
    register ssize_t __retval asm("eax");\
    if((num)<128)\
        asm volatile("push $" STRINGIFY(num) "\npop %%eax\nint $0x80\n"\
            :"=a" (__retval) :"b" (a), "c" (b), "d" (c), "S" (d), "D" (e):); \
    else\
        asm volatile("mov $"  STRINGIFY(num) ", %%eax\nint $0x80\n"\
            :"=a" (__retval) :"b" (a), "c" (b), "d" (c), "S" (d), "D" (e):); \
    __retval; \
})\

#define SYS_syscall6(num, a, b, c, d, e, f) ({\
    register ssize_t __retval asm("eax");\
    if((num)<128)\
        asm volatile( \
            "mov %%eax, %%ebp\n" \
            "push $" STRINGIFY(num) "\npop %%eax\n" \
            "int $0x80\n"\
            :"=a" (__retval) \
            :"b" (a), "c" (b), "d" (c), "S" (d), "D" (e), "a" (f) :"ebp"); \
    else\
        asm volatile( \
            "mov %%eax, %%ebp\n" \
            "mov $"  STRINGIFY(num) ", %%eax\n" \
            "int $0x80\n"\
            :"=a" (__retval) \
            :"b" (a), "c" (b), "d" (c), "S" (d), "D" (e), "a" (f) :"ebp"); \
    __retval; \
})\

#endif /* x86_64/i386 */

// -- syscall numbers

#ifdef __x86_64__
#define SYS_NR_read 0
#define SYS_NR_write 1
#define SYS_NR_open 2
#define SYS_NR_close 3
#define SYS_NR_stat 4
#define SYS_NR_fstat 5
#define SYS_NR_poll 7
#define SYS_NR_lseek 8
#define SYS_NR_mmap 9
#define SYS_NR_munmap 11
#define SYS_NR_brk 12
#define SYS_NR_ioctl 16
#define SYS_NR_pipe 22
#define SYS_NR_dup2 33
#define SYS_NR_pause 34
#define SYS_NR_clone 56
#define SYS_NR_fork 57
#define SYS_NR_vfork 58
#define SYS_NR_execve 59
#define SYS_NR_exit 60
#define SYS_NR_kill 62
#define SYS_NR_fcntl 72
#define SYS_NR_clock_gettime 228
#define SYS_NR_clock_nanosleep 230
#define SYS_NR_exit_group 231
#define SYS_NR_waitid 247
#define SYS_NR_dup3 292
#define SYS_NR_pipe2 293
#define SYS_NR_memfd_create 319
#define SYS_NR_execveat 322
#elif defined(__i386__)
#define SYS_NR_exit 1
#define SYS_NR_fork 2
#define SYS_NR_read 3
#define SYS_NR_write 4
#define SYS_NR_open 5
#define SYS_NR_close 6
#define SYS_NR_waitpid 7
#define SYS_NR_execve 11
#define SYS_NR_lseek 19
#define SYS_NR_pause 29
#define SYS_NR_kill 37
#define SYS_NR_pipe 42
#define SYS_NR_brk 45
#define SYS_NR_ioctl 54
#define SYS_NR_fcntl 55
#define SYS_NR_dup2 63
#define SYS_NR_mmap 90
#define SYS_NR_munmap 91
#define SYS_NR_stat 106
#define SYS_NR_fstat 108
#define SYS_NR_clone 120
#define SYS_NR_poll 168
#define SYS_NR_vfork 190
#define SYS_NR_exit_group 252
#define SYS_NR_clock_gettime 265
#define SYS_NR_clock_nanosleep 267
#define SYS_NR_waitid 284
#define SYS_NR_dup3 330
#define SYS_NR_pipe2 331
#define SYS_NR_memfd_create 356
#define SYS_NR_execveat 358
#endif

// --- actual syscalls

SYSCALL_FUNC ssize_t SYS_stat(const char* path, struct stat* st) {
    register ssize_t r = SYS_syscall2(SYS_NR_stat, path, st);
    MEMORY_BARRIER();
    return r;
}
SYSCALL_FUNC ssize_t SYS_fstat(int fd, struct stat* st) {
    register ssize_t r = SYS_syscall2(SYS_NR_fstat, fd, st);
    MEMORY_BARRIER();
    return r;
}
SYSCALL_FUNC ssize_t SYS_write(int fd, const void* buf, size_t sz) {
    return SYS_syscall3(SYS_NR_write, fd, buf, sz);
}
SYSCALL_FUNC ssize_t SYS_read(int fd, void* buf, size_t sz) {
    register ssize_t r = SYS_syscall3(SYS_NR_read, fd, buf, sz);
    MEMORY_BARRIER();
    return r;
}
SYSCALL_FUNC int SYS_open(const char* path, int flags) {
    return (int)SYS_syscall2(SYS_NR_open, path, flags);
}
SYSCALL_FUNC int SYS_close(int fd) {
    return (int)SYS_syscall1(SYS_NR_close, fd);
}
SYSCALL_FUNC ssize_t SYS_fcntl(int fd, size_t cmd, size_t arg) {
    return SYS_syscall3(SYS_NR_fcntl, fd, cmd, arg);
}
SYSCALL_FUNC ssize_t SYS_ioctl(int fd, size_t cmd, void* arg) {
    return SYS_syscall3(SYS_NR_ioctl, fd, cmd, arg);
}
SYSCALL_FUNC ssize_t SYS_munmap(void* addr, size_t sz) {
    register ssize_t r = SYS_syscall2(SYS_NR_munmap, addr, sz);
    MEMORY_BARRIER();
    return r;
}
SYSCALL_FUNC ssize_t SYS_clock_gettime(clockid_t clkid, struct timespec* t) {
    register ssize_t r = SYS_syscall2(SYS_NR_clock_gettime, clkid, t);
    MEMORY_BARRIER();
    return r;
}
SYSCALL_FUNC ssize_t SYS_clock_nanosleep(clockid_t clkid, int flags,
        const struct timespec* t, struct timespec* remain) {
    register ssize_t r = SYS_syscall4(SYS_NR_clock_nanosleep, clkid, flags, t, remain);
    MEMORY_BARRIER();
    return r;
}
SYSCALL_FUNC pid_t SYS_fork(void) {
    return (pid_t)SYS_syscall0(SYS_NR_fork);
}
SYSCALL_FUNC pid_t SYS_vfork(void) {
    return (pid_t)SYS_syscall0(SYS_NR_vfork);
}
/*
 * NO _SETTLS, _PARENT_SETTID _CHILD_*TID
 * 64:
 *mmap(NULL, 8392704, PROT_NONE, MAP_PRIVATE|MAP_ANONYMOUS|MAP_STACK, -1, 0) = 0x62451d9f7000
 *clone(child_stack=0x62451e1f6fb0,
        flags=CLONE_VM|CLONE_FS|CLONE_FILES|CLONE_SIGHAND|CLONE_THREAD|CLONE_SYSVSEM|CLONE_SETTLS|CLONE_PARENT_SETTID|CLONE_CHILD_CLEARTID,
		parent_tidptr=0x62451e1f79d0,
		tls=0x62451e1f7700,
		child_tidptr=0x62451e1f79d0) = 2669
 *flags child_stack ptid ctid newtls

 * 32:
 *mmap2(NULL, 8392704, PROT_NONE, MAP_PRIVATE|MAP_ANONYMOUS|MAP_STACK, -1, 0) = 0xed02b000
 *clone(child_stack=0xed82b424,
		flags=CLONE_VM|CLONE_FS|CLONE_FILES|CLONE_SIGHAND|CLONE_THREAD|CLONE_SYSVSEM|CLONE_SETTLS|CLONE_PARENT_SETTID|CLONE_CHILD_CLEARTID,
		parent_tidptr=0xed82bba8,
		tls={entry_number=12, base_addr=0xed82bb40, limit=0x0fffff,
			 seg_32bit=1, contents=0, read_exec_only=0, limit_in_pages=1,
			 seg_not_present=0, useable=1},
		child_tidptr=0xed82bba8) = 6107
 *flags child_stack ptid newtls ctid
 */
/*SYSCALL_FUNC int SYS_clone(int (*fn)(void*), void* chld_stack,
		int flags, void* arg, )*/
SYSCALL_FUNC ssize_t SYS_pipe(int fd[2]) {
    register ssize_t r = SYS_syscall1(SYS_NR_pipe, (int*)fd);
    MEMORY_BARRIER();
    return r;
}
SYSCALL_FUNC ssize_t SYS_pipe2(int fd[2], int flags) {
    register ssize_t r = SYS_syscall2(SYS_NR_pipe2, (int*)fd, flags);
    MEMORY_BARRIER();
    return r;
}
SYSCALL_FUNC ssize_t SYS_kill(pid_t pid, int sig) {
    return SYS_syscall2(SYS_NR_kill, pid, sig);
}
SYSCALL_FUNC ssize_t SYS_dup2(int oldfd, int newfd) {
    return SYS_syscall2(SYS_NR_dup2, oldfd, newfd);
}
SYSCALL_FUNC ssize_t SYS_dup3(int oldfd, int newfd, int flags) {
    return SYS_syscall3(SYS_NR_dup3, oldfd, newfd, flags);
}
SYSCALL_FUNC int SYS_memfd_create(const char* name, unsigned int flags) {
    return (int)SYS_syscall2(SYS_NR_memfd_create, name, flags);
}
__attribute__((__noreturn__)) SYSCALL_FUNC
void SYS_execve(const char* file, char* argv[], char* envp[]) {
    SYS_syscall3(SYS_NR_execve, file, argv, envp);
    __builtin_unreachable();
}
__attribute__((__noreturn__)) SYSCALL_FUNC
void SYS_execvec(const char* file, const char*const* argv
        /*char*const argv[]*/, const char*const envp[]) {
    SYS_syscall3(SYS_NR_execve, file, argv, envp);
    __builtin_unreachable();
}
SYSCALL_FUNC ssize_t SYS_poll(struct pollfd* fds, nfds_t nfds, int timeout) {
    register ssize_t r = SYS_syscall3(SYS_NR_poll, fds, nfds, timeout);
    MEMORY_BARRIER();
    return r;
}
SYSCALL_FUNC ssize_t SYS_lseek(int fd, off_t offset, int whence) {
    return SYS_syscall3(SYS_NR_lseek, fd, offset, whence);
}
SYSCALL_FUNC void SYS_pause(void) {
    SYS_syscall0(SYS_NR_pause);
}
__attribute__((__noreturn__)) SYSCALL_FUNC
void SYS_execveat(int dirfd, const char* path, char* argv[],
        char* envp[], int flags) {
    SYS_syscall5(SYS_NR_execveat, dirfd, path, argv, envp, flags);
    __builtin_unreachable();
}
__attribute__((__noreturn__)) SYSCALL_FUNC
void SYS_execveatc(int dirfd, const char* path, const char*const* argv,
        const char*const envp[], int flags) {
    SYS_syscall5(SYS_NR_execveat, dirfd, path, argv, envp, flags);
    __builtin_unreachable();
}
__attribute__((__noreturn__)) SYSCALL_FUNC void SYS_exit(int c) {
    SYS_syscall1(SYS_NR_exit, c);
    __builtin_unreachable();
}
__attribute__((__noreturn__)) SYSCALL_FUNC void SYS_exit_group(int c) {
    SYS_syscall1(SYS_NR_exit_group, c);
    __builtin_unreachable();
}
SYSCALL_FUNC ssize_t SYS_brk(void* p) {
    return SYS_syscall1(SYS_NR_brk, p);
}
SYSCALL_FUNC ssize_t SYS_waitid(idtype_t idtype, id_t id, siginfo_t *infop, int options) {
    return SYS_syscall4(SYS_NR_waitid, idtype, id, infop, options);
}
#ifdef __i386__
SYSCALL_FUNC ssize_t SYS_waitpid(pid_t pid, int* wstat, int opt) {
    return SYS_syscall3(SYS_NR_waitpid, pid, wstat, opt);
}
#else
__attribute__((__deprecated__("waitpid(2) isn't directly available as a "\
    "syscall on this platform. Consider using waitid(2) instead.")))
force_inline static ssize_t SYS_waitpid(pid_t pid, int* wstat, int opt) {
    // TODO: write to wstat
    siginfo_t si;
    ssize_t r = SYS_waitid(pid <= (si.si_pid = 0) ? P_ALL : P_PID, (id_t)pid,
        &si, opt);
    return r < 0 ? r : si.si_pid;
}
#endif

// --- special cases: mmap

// go home clang, you're drunk
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wunused-parameter"
// reduced version because reasons
SYSCALL_FUNC void* SYS_mmap(void* addr, size_t len, int filed) {
#pragma clang diagnostic pop
    register ssize_t retval
#if defined(__x86_64__)
        asm("rax");

    asm volatile("mov %[filedr], %%r8\n"
                 "mov %%r8, %%rdx\n"
                 "mov %%rdx, %%r10\n"
                 "xorl %%r9, %%r9\n"
                 "push $9\n"
                 "pop %%rax\n"
                 "syscall\n"
        :"=a" (retval)
        :"D" (addr), "S" (len), [filedr] "r" (filed)
        :"rcx","r11","r10","r8","r9");
#elif defined(__i386__)
        asm("eax");

    asm volatile("push %%ebx\n"
                 "xorl %%ebx, %%ebx\n"
                 "push %%ebx\n"
                 "push %%edx\n" // edx == 3
                 "push %%edx\n" // 1|_ -> MAP_SHARED
                 "push %%edx\n" // 3 -> PROT_READ|PROT_WRITE
                 "push %%eax\n"
                 "push %%ebx\n"
                 "push $90\n"
                 "pop %%eax\n"
                 "mov %%esp, %%ebx\n"
                 "int $0x80\n"
                 "pop %%ebx\n"
                 "pop %%ebx\n"
                 "pop %%ebx\n"
                 "pop %%ebx\n"
                 "pop %%ebx\n"
                 "pop %%ebx\n"
                 "pop %%ebx\n"
        :"=a" (retval)
        :"a" (len), "d" (filed)
        :);
#endif

    return (void*)retval;
}

#endif /* SYS_H_ */

