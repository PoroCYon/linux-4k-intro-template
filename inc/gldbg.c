// vim: set ft=c:

#ifdef VERBOSE
static void APIENTRY gldbg_cb(GLenum src, GLenum type, GLuint id,
        GLenum sev, GLsizei len, const GLchar* msg, const void* ud) {
    /*if (sev != GL_DEBUG_SEVERITY_HIGH_ARB && sev != GL_DEBUG_SEVERITY_MEDIUM_ARB
            && sev != GL_DEBUG_SEVERITY_LOW_ARB) {
        return;
    }*/

    dprintf(STDERR_FILENO, "GL debug ");
    switch (src) {
        case GL_DEBUG_SOURCE_API_ARB: dprintf(STDERR_FILENO, "api "); break;
        case GL_DEBUG_SOURCE_WINDOW_SYSTEM_ARB: dprintf(STDERR_FILENO, "wnd sys "); break;
        case GL_DEBUG_SOURCE_SHADER_COMPILER_ARB: dprintf(STDERR_FILENO, "glslc "); break;
        case GL_DEBUG_SOURCE_THIRD_PARTY_ARB: dprintf(STDERR_FILENO, "3rd "); break;
        case GL_DEBUG_SOURCE_APPLICATION_ARB: dprintf(STDERR_FILENO, "appl "); break;
        case GL_DEBUG_SOURCE_OTHER_ARB: dprintf(STDERR_FILENO, "other-s "); break;
    }

    switch (type) {
        case GL_DEBUG_TYPE_ERROR_ARB: dprintf(STDERR_FILENO, "err "); break;
        case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR_ARB: dprintf(STDERR_FILENO, "depr "); break;
        case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR_ARB: dprintf(STDERR_FILENO, "undef "); break;
        case GL_DEBUG_TYPE_PORTABILITY_ARB: dprintf(STDERR_FILENO, "port "); break;
        case GL_DEBUG_TYPE_PERFORMANCE_ARB: dprintf(STDERR_FILENO, "perf "); break;
        case GL_DEBUG_TYPE_OTHER_ARB: dprintf(STDERR_FILENO, "other-t "); break;
    }

    switch (sev) {
        case GL_DEBUG_SEVERITY_HIGH_ARB: dprintf(STDERR_FILENO, "high"); break;
        case GL_DEBUG_SEVERITY_MEDIUM_ARB: dprintf(STDERR_FILENO, "med"); break;
        case GL_DEBUG_SEVERITY_LOW_ARB: dprintf(STDERR_FILENO, "low"); break;
        case GL_DEBUG_SEVERITY_NOTIFICATION: dprintf(STDERR_FILENO, "note "); break;
        default: dprintf(STDERR_FILENO, "note?"); break;
    }

    dprintf(STDERR_FILENO, ": %i (0x%X): %s\n", id, id, msg);

#ifdef DEBUG
    if (type == GL_DEBUG_TYPE_ERROR_ARB || sev == GL_DEBUG_SEVERITY_HIGH_ARB) {
        SYS_break();
    }
#endif
}
#endif


