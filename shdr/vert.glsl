#version 430 core

vec2 y = vec2(1., -1.);
vec4 x[4] = {y.yyxx,y.xyxx,y.yxxx,y.xxxx};

void main() {
	gl_Position = x[gl_VertexID];
}

