
Player source code and usage example.

In order to build, you need to set up a rule in Visual Studio to compile
.asm files. I can recommend Yasm, which has nice Visual Studio integration
plus debugging support, but Nasm works as well.
