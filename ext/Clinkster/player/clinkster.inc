
%include "platform.inc"

extern PUBLIC_FN(Clinkster_GenerateMusic,0)
extern PUBLIC_FN(Clinkster_StartMusic,0)
extern PUBLIC_FN(Clinkster_GetPosition,0)
extern PUBLIC_FN(Clinkster_GetInstrumentTrigger,8)

extern PUBLIC_DATA(Clinkster_MusicBuffer)
extern PUBLIC_DATA(Clinkster_TicksPerSecond)
extern PUBLIC_DATA(Clinkster_MusicLength)
extern PUBLIC_DATA(Clinkster_NumTracks)
%if CLINKSTER_GENERATE_TIMING_DATA
extern PUBLIC_DATA(Clinkster_NoteTiming)
%endif
extern PUBLIC_DATA(Clinkster_WavFileHeader)

